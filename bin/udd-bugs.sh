#!/bin/sh
#
# Script from Helmut - check UDD for FTBFS bugs and print some details

udd() {
        PGPASSWORD=udd-mirror psql --host=udd-mirror.debian.net --user=udd-mirror udd "$@"
}

udd -c "\\COPY (SELECT id, package, affected_packages, title FROM all_bugs WHERE (EXISTS (SELECT 1 FROM bugs_tags WHERE bugs_tags.id = all_bugs.id AND tag = 'ftbfs') OR (title LIKE '%FTBFS%' AND package LIKE 'src:%')) AND severity IN ('serious', 'critical', 'grave') AND affects_unstable = 't') TO STDOUT WITH CSV DELIMITER '_';" |
while IFS='_' read bugnum pkg affects title; do
        affects="${affects#\"}"
        affects="${affects%\"}"
        case "$pkg" in
                src:*)
                        source=${pkg#src:}
                        echo "$source $bugnum $title"
                ;;
                *)
                        echo "$affects" | tr ',' '\n' | while read affected; do
                                test -z "$affected" && continue
                                source=${affected#src:}
                                test "$affected" = "$source" && continue
                                echo "$source $bugnum $title"
                        done
                ;;
        esac
done | sort
